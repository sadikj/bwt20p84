const Sequelize = require('sequelize'); 
const sequelize = new Sequelize('wt2084-st', 'root', 'root', { 
   host: 'localhost', 
   dialect: 'mysql', 
   pool: { 
       max: 5, 
       min: 0, 
       acquire: 30000, 
       idle: 10000 
   } 
}); 

const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;

db.Predmet = sequelize.define('predmet', {
    naziv: Sequelize.STRING
});

db.Grupa = sequelize.define('grupa', {
    naziv: Sequelize.STRING
});

db.Aktivnost = sequelize.define('aktivnost', {
    naziv: Sequelize.STRING,
    pocetak: Sequelize.FLOAT,
    kraj: Sequelize.FLOAT
});

db.Dan = sequelize.define('dan', {
    naziv: Sequelize.STRING
});

db.Tip = sequelize.define('tip', {
    naziv: Sequelize.STRING
});

db.Student = sequelize.define('student', {
    ime: Sequelize.STRING,
    index: Sequelize.STRING
});

//Predmet 1-N Grupa 
db.Predmet.hasMany(db.Grupa,{as:'predmetGrupa'});

//Aktivnost N-1 Predmet
db.Predmet.hasMany(db.Aktivnost,{as:'predmetAktivnost'});

//Aktivnost N-0 Grupa
db.Grupa.hasMany(db.Aktivnost, {as:'grupaAktivnost'});

//Aktivnost N-1 Dan
db.Dan.hasMany(db.Aktivnost,{as:'danAktivnost'});

//Aktivnost N-1 Tip
db.Tip.hasMany(db.Aktivnost, {as:'tipAktivnost'});

//Student N-M Grupa
db.grupa_student = db.Student.belongsToMany(db.Grupa, {as: 'grupaStudent', through: 'grupa_student', foreignKey: 'studentId'});
db.Grupa.belongsToMany(db.Student, {as: 'studentiGrupa', through: 'grupa_student', foreignKey: 'grupaId'});

module.exports=db;