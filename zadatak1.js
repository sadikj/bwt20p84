const http = require('http');
const fs = require('fs');
const url = require('url'); 
const querystring = require('querystring');
const bodyParser = require('body-parser');

const express = require('express');const { posix } = require('path');
const db = require('./baza.js');
const { PassThrough } = require('stream');

const app = express(); 
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(express.static('public'));


async function generisiPodatkeZaBazu(){
    let predmet = [];
    predmet.push(db.Predmet.create({naziv: `RAM`}));
    predmet.push(db.Predmet.create({naziv: `BWT`}));
    predmet.push(db.Predmet.create({naziv: `ICR`}));
    await Promise.all(predmet).catch(function(err){console.log(err);});
    let grupa = [];
    grupa.push(db.Grupa.create({naziv: `RAMgrupa1`, predmetId: `1`}));
    grupa.push(db.Grupa.create({naziv: `RAMgrupa2`, predmetId: `1`}));
    grupa.push(db.Grupa.create({naziv: `BWTgrupa1`, predmetId: `2`}));
    grupa.push(db.Grupa.create({naziv: `BWTgrupa2`, predmetId: `2`}));
    grupa.push(db.Grupa.create({naziv: `ICRgrupa1`, predmetId: `3`}));
    grupa.push(db.Grupa.create({naziv: `ICRgrupa2`, predmetId: `3`}));
    await Promise.all(grupa).catch(function(err){console.log(err);});
    let dan = [];
    dan.push(db.Dan.create({naziv: `ponedjeljak`}));
    dan.push(db.Dan.create({naziv: `utorak`}));
    dan.push(db.Dan.create({naziv: `srijeda`}));
    await Promise.all(dan).catch(function(err){console.log(err);});
    let tip = [];
    tip.push(db.Tip.create({naziv: `predavanje`}));
    tip.push(db.Tip.create({naziv: `vjezbe`}));
    tip.push(db.Tip.create({naziv: `tutorijal`}));
    await Promise.all(tip).catch(function(err){console.log(err);});
    let student = [];
    student.push(db.Student.create({ime: `Meho Mehic`, index: `45`}));
    student.push(db.Student.create({ime: `Kemo Kemic`, index: `50`}));
    student.push(db.Student.create({ime: `Fata Fatic`, index: `55`}));
    student.push(db.Student.create({ime: `Mujo Mujic`, index: `60`}));
    await Promise.all(student).catch(function(err){console.log(err);});
    let aktivnost = [];
    aktivnost.push(db.Aktivnost.create({naziv: `RAMpredavanje`, pocetak: `9.0`, kraj: `10.0`, predmetId: `1`, grupaId: `1`, danId: `1`, tipId: `1` }));
    await Promise.all(aktivnost).catch(function(err){console.log(err);});
}


db.sequelize.sync({force:true}).then(function(){
    generisiPodatkeZaBazu();
    console.log("Gotovo kreiranje tabela i ubacivanje pocetnih podataka!");
    //process.exit();
});

const jsonRez = [{"greska":"Datoteka raspored.csv nije kreirana!"}];
let greska = "Datoteka raspored.csv nije kreirana!";


function validirajNazivPredmeta(nazivpredmeta){
    if(nazivpredmeta.match(/^[A-Z]+[0-9]*-grupa[1-9]+$/g)) return true;
    if(nazivpredmeta.match(/^[A-Z]+[0-9]*$/g)) return true;
    return false;
}
function validirajAktivnost(aktivnost){
    aktivnost = aktivnost.toLowerCase();
    if(aktivnost==="predavanje" || aktivnost==="vjezbe" || aktivnost==="vježbe")
    return true;
    else return false;
}
function validirajDan(dan){
    dan = dan.toLowerCase();
    if(dan==="ponedjeljak" || dan==="utorak" || dan==="srijeda" || dan==="cetvrtak" || dan==="četvrtak"
       || dan==="petak" || dan==="subota" || dan==="nedjelja")
       return true;
    else return false;
}
function validirajVrijeme(datum){
    if(datum.match(/^[0-9]+:[0-9]+$/g)) {
        let vrijeme = datum.split(":");
        if(((vrijeme[0]*1)>=0) &&  ((vrijeme[0]*1)<24) && ((vrijeme[1]*1)>=0) &&  ((vrijeme[1]*1)<60)) return true;
    }
    return false;
}
function uzmiPodatke(){
    let rez = [];
    let data = "";
    try{
        data = fs.readFileSync('public/raspored.csv');
    }
    catch(err){
        return jsonRez;
    }
        let datoteka =  data.toString();
        let razPoRedu = datoteka.split("\n");
        let headeri = ["naziv","aktivnost","dan","pocetak","kraj"];
        for(let i=0; i<razPoRedu.length; i++){
            let razdPoZar = razPoRedu[i].split(",");
            let objekat={};
            for(let j=0; j<razdPoZar.length; j++){
                objekat[headeri[j].trim()] = razdPoZar[j].trim();
            }
            rez.push(objekat);
        }
    return rez;
}
function vratiObicno(){
    let data = "";
    try{
        data = fs.readFileSync('public/raspored.csv');
    }
    catch(err){
        return greska;
    }
    return data;
}

function sviPredmeti(){
    let data = "";
    try{
        data = fs.readFileSync('public/predmeti.csv');
    }
    catch(err){
        return greska;
    }
    return data;
}

function dohvatiIstiDan(dan){
    let rez = uzmiPodatke();
    if(rez===jsonRez) return jsonRez;
    let konacniRez = [];
    for(let i=0; i<rez.length; i++){
        if(rez[i].dan===dan){
            konacniRez.push(rez[i]);
        }
    }
    return konacniRez;
}
function izJSONUCSV(rez){
    if(rez===jsonRez) return greska;
    let konacniRez = "";
    for(let i=0; i<rez.length; i++){
        konacniRez = konacniRez+rez[i].naziv+',';
        konacniRez = konacniRez+rez[i].aktivnost+',';
        konacniRez = konacniRez+rez[i].dan+',';
        konacniRez = konacniRez+rez[i].pocetak+',';
        konacniRez = konacniRez+rez[i].kraj;
        konacniRez = konacniRez+'\n';
    } 
    return konacniRez;
}
function sortirajPoHederuA(atribut){  
    return function(prvi,drugi){  
       if(prvi[atribut] > drugi[atribut])  return 1;  
       else if(prvi[atribut] < drugi[atribut])  return -1;  
       return 0;  
    }  
 }
 function sortirajPoHederuD(atribut){  
    return function(prvi,drugi){  
       if(prvi[atribut] < drugi[atribut])  return 1;  
       else if(prvi[atribut] > drugi[atribut])  return -1;  
       return 0;  
    }  
 }

 function vratiBroj(atribut){
     if(atribut === "ponedjeljak") return 1;
     else if (atribut === "utorak") return 2;
     else if (atribut === "srijeda") return 3;
     else if (atribut ==="cetvrtak" || atribut === "četvrtak") return 4;
     else if (atribut === "petak") return 5;
     else if (atribut === "subota") return 6;
     else if (atribut === "nedjelja") return 7;
 }
 function sortirajDanA(atribut){
    return function(prvi,drugi){  
        let prviBr = vratiBroj(prvi[atribut]);
        let drugiBr = vratiBroj(drugi[atribut]);
        if(prviBr > drugiBr)  return 1;  
        else if(prviBr < drugiBr)  return -1;  
        return 0;  
     }
 }

 function sortirajDanD(atribut){
    return function(prvi,drugi){  
        let prviBr = vratiBroj(prvi[atribut]);
        let drugiBr = vratiBroj(drugi[atribut]);
        if(prviBr < drugiBr)  return 1;  
        else if(prviBr > drugiBr)  return -1;  
        return 0;  
     }
}


 function vratiMinute(vrijeme){
    let minute = vrijeme.split(':');
    return minute[0]*60 + minute[1]*1;
 }

 app.get('/v1/raspored',function(req,res){ 
    const urlPut = url.parse(req.url);   
    if(urlPut.query!==null && urlPut.pathname==='/v1/raspored'){
        let jsonUrl = url.parse(req.url,true).query; 
        let rezultat = "";
        if(urlPut.query.includes("dan")){
                let dan = jsonUrl.dan;
                rezultat = dohvatiIstiDan(dan);
        }
        if(urlPut.query.includes("sort")){
            if(urlPut.query.includes("dan=")){
                let znak = jsonUrl.sort.charAt(0);
                let pretraga = jsonUrl.sort.replace(znak,'');
                if(jsonUrl.sort === "Adan" || jsonUrl.sort === "Ddan"){
                    if(znak==='A')
                    rezultat = rezultat.sort(sortirajDanA(pretraga));
                    else if(znak==='D')
                    rezultat = rezultat.sort(sortirajDanD(pretraga));
                }
                else{
                    if(znak==='A')
                    rezultat = rezultat.sort(sortirajPoHederuA(pretraga));
                    else if(znak==='D')
                    rezultat = rezultat.sort(sortirajPoHederuD(pretraga));
                }
            }
            else{
                rezultat = uzmiPodatke();
                let znak = jsonUrl.sort.charAt(0);
                let pretraga = jsonUrl.sort.replace(znak,'');
                if(jsonUrl.sort === "Adan" || jsonUrl.sort === "Ddan"){
                    if(znak==='A')
                    rezultat = rezultat.sort(sortirajDanA(pretraga));
                    else if(znak==='D')
                    rezultat = rezultat.sort(sortirajDanD(pretraga));
                }
                else{
                    if(znak==='A')
                    rezultat = rezultat.sort(sortirajPoHederuA(pretraga));
                    else if(znak==='D')
                    rezultat = rezultat.sort(sortirajPoHederuD(pretraga));
                }
            }
        }
        //let varijsss = "";
        if(JSON.stringify(req.headers.accept)==="\"text/csv\""){
            res.writeHead(200,{
                'Accept': 'text/csv'
            });
            rezultat = izJSONUCSV(rezultat);
            res.end(rezultat.toString());
        }
        else{
            res.writeHead(200,{
                'Content-Type': 'application/json'
            });
            res.end(JSON.stringify(rezultat));
        }

    }
    else if(urlPut.pathname==='/v1/raspored'){
        if(JSON.stringify(req.headers.accept)==="\"text/csv\""){
            res.writeHead(200,{
                'Accept': 'text/csv'
            });
            let jsonData = vratiObicno();
            res.end(jsonData.toString());
        }
        else{
            res.writeHead(200,{
                'Content-Type': 'application/json'
            });
            let jsonData = uzmiPodatke();
            res.end(JSON.stringify(jsonData));
        }
    }
    else{
        res.writeHead(404);
        res.end();
    }
});

app.post("/v1/raspored", function(req, res){
    const urlPut = url.parse(req.url);
    let data = req.body;
    if(validirajNazivPredmeta(data['nazivpredmeta'])===true && validirajAktivnost(data['aktivnost'])===true
        && validirajDan(data['dan'])===true && validirajVrijeme(data['vrijemepocetka'])===true && 
        validirajVrijeme(data['vrijemekraja'])===true){
        res.writeHead(200,{
            'Content-Type': 'text/plain; charset=utf-8'
        });
        let objekat = uzmiPodatke();
        let brojacTacnihPocetak = 0;
        let brojacTacnihKraj = 0;
        for(let i=0; i<objekat.length;i++){
            if(data['dan'].toLowerCase()===objekat[i].dan.toLowerCase() && (vratiMinute(data['vrijemepocetka'])>=vratiMinute(objekat[i].pocetak) && vratiMinute(data['vrijemepocetka'])<vratiMinute(objekat[i].kraj))){
                brojacTacnihPocetak = brojacTacnihPocetak + 1;
            }
            if(data['dan'].toLowerCase()===objekat[i].dan.toLowerCase() && (vratiMinute(data['vrijemekraja'])>=vratiMinute(objekat[i].pocetak) && vratiMinute(data['vrijemekraja'])<vratiMinute(objekat[i].kraj))){
                brojacTacnihKraj = brojacTacnihKraj + 1;
            }
        }
                
        if(brojacTacnihPocetak===0 && brojacTacnihKraj===0){
            fs.appendFileSync('public/raspored.csv',`\n${data['nazivpredmeta']},${data['aktivnost'].toLowerCase()},${data['dan'].toLowerCase()},${data['vrijemepocetka']},${data['vrijemekraja']}`,'UTF-8');
            res.end('Uspješno je dodana aktivnost.');
        }
        else{
             res.end('Aktivnost nije dodana, pokušajte ponovo.');
        }
    }
    else{
        res.writeHead(200,{
            'Content-Type': 'text/plain; charset=utf-8'
        });
        res.end('Aktivnost nije dodana, pokušajte ponovo.');
    }
});

app.post("/v1/predmet", function(req, res){
    let spisakPredmeta = sviPredmeti();
    spisakPredmeta = spisakPredmeta.toString().split("\n");
    let data = req.body;
    let postoji = false;
    for(let i=0; i<spisakPredmeta.length; i++){
        if(spisakPredmeta[i]===data['nazivpredmeta']){
            postoji = true;
        }
    }
    res.writeHead(200,{
        'Content-Type': 'application/json'
    });
    if(postoji === false) {
        let stringRezultat = `\n${data["nazivpredmeta"]}`;
        try{
            fs.appendFileSync("public/predmeti.csv", stringRezultat);
        }
        catch(err){
            return jsonRez;
        }
    }
    res.end(JSON.stringify([{"message" : "Predmet je uspjesno kreiran ili je postojao u listi predmeta"}]));
});

app.delete("/v1/predmet", function(req, res){
    const urlPut = url.parse(req.url);
    let vr = urlPut.query;
    vr = vr.replace("naziv=","");
    let naziv = vr;
    let spisakPredmeta = sviPredmeti();
    spisakPredmeta = spisakPredmeta.toString().split("\n");
    for(let i=0; i<spisakPredmeta.length; i++){
        spisakPredmeta[i] =  spisakPredmeta[i].replace("\r", "");
    }
    let rezultatPovratni = [];
    for(let i=0; i<spisakPredmeta.length; i++){
        if(spisakPredmeta[i].localeCompare(naziv) !==0){
            rezultatPovratni.push(spisakPredmeta[i]);
        }
    }
    res.writeHead(200,{
        'Content-Type': 'application/json'
    });
    try{
        fs.writeFileSync("public/predmeti.csv", "");
    }
    catch(err){
        res.end(JSON.stringify([{message:"Greška - predmet nije obrisan!"}]));
    }
    for(let i=0; i<rezultatPovratni.length; i++){
        try{
            if(i===0){
                fs.appendFileSync("public/predmeti.csv", rezultatPovratni[i]);
            }
            else{
                fs.appendFileSync("public/predmeti.csv", "\n"+rezultatPovratni[i]);
            }
        }
        catch(err){
            res.end(JSON.stringify([{message:"Greška - predmet nije obrisan!"}]));
        }
    }
    res.end(JSON.stringify([{message:"Uspješno obrisana aktivnost!"}]));
});

app.get("/v1/predmet",function(req,res){
    res.writeHead(200,{
        'Content-Type': 'application/json'
    });
    let spisakPredmeta = sviPredmeti();
    spisakPredmeta = spisakPredmeta.toString().split("\n");
    for(let i=0; i<spisakPredmeta.length; i++){
        spisakPredmeta[i] =  spisakPredmeta[i].replace("\r", "");
    }
    let rezultat = [];
    for(let i=0; i<spisakPredmeta.length; i++){
        let objekat = {};
        objekat["naziv"] = spisakPredmeta[i];
        rezultat.push(objekat);
    }
    res.end(JSON.stringify(rezultat));
});







app.post("/v2/predmet", function(req, res){
    let data = req.body;
    let predmet = [];
    if(data['naziv']===undefined){
        res.end("Unesite naziv predmeta");
    }
    else{
        predmet.push(db.Predmet.create({naziv: `${data['naziv']}`}));
        Promise.all(predmet).then(res.end("Uspjesno ste dodali novi predmet")).catch(function(err){console.log(err);});
    }
});

app.get("/v2/predmet", function(req, res){
    db.Predmet.findAll().then(function(predmeti){
        res.end(JSON.stringify(predmeti));
    });
});


app.delete("/v2/predmet", function(req, res){
    db.Predmet.findOne({where: {id: `${req.query.id}`}}).then(function(predmeti){
        if(predmeti!==null){
            predmeti.destroy();
            res.end("Uspjesno ste obrisali predmet");
        }
        else{
            res.end("Predmet koji pokusavate da obrisete nije u tabeli");
        }
    });
});
app.put("/v2/predmet", function(req, res){
    let data = req.body;
    db.Predmet.findOne({where: {id: `${req.query.id}`}}).then(async function(predmeti){
        if(predmeti!==null){
            if(data['naziv']!==undefined){
                predmeti.naziv = data['naziv'];
                await predmeti.save();
                res.end("Uspjesno ste izvrisili update predmeta");                                    
            }
            else(res.end("Unesite naziv predmeta"));
        }
        else{
            res.end("Predmet koji pokusavate da update-ovati nije u tabeli");
        }
    });
});






app.post("/v2/grupa", function(req, res){
    let data = req.body;
    let grupa = [];
    grupa.push(db.Grupa.create({naziv: `${data['naziv']}`, predmetId: `${data['predmet']}`}));
    Promise.all(grupa).then(res.end("Uspjesno ste dodali novu grupu")).catch(function(err){console.log(err);});
});

app.get("/v2/grupa", function(req, res){
    let id = req.query.id;
    if(id===undefined){
        db.Grupa.findAll().then(function(grupa){
            res.end(JSON.stringify(grupa));
        });
    }
    else{
        db.Grupa.findOne({where: {id: `${req.query.id}`}}).then(function(grupa){
            res.end(JSON.stringify(grupa));
        });
    }
});

app.delete("/v2/grupa", function(req, res){
    db.Grupa.findOne({where: {id: `${req.query.id}`}}).then(function(grupa){
        if(grupa!==null){
            grupa.destroy();
            res.end("Uspjesno ste obrisali grupu");
        }
        else{
            res.end("Grupa koju pokusavate da obrisete nije u tabeli");
        }
    });
});
app.put("/v2/grupa", function(req, res){
    let data = req.body;
    db.Grupa.findOne({where: {id: `${req.query.id}`}}).then(async function(grupa){
        if(grupa!==null){
            if(data['naziv']!==undefined){
                grupa.naziv = data['naziv'];
                await grupa.save();
            }
            if(data['predmetId']!==undefined){
                grupa.predmetId = data['predmet'];
                await grupa.save();
            }
            res.end("Uspjesno ste izvrisili update grupe");
        }
        else{
            res.end("Grupu koji pokusavate da update-ovati nije u tabeli");
        }
    });
});






app.post("/v2/aktivnost", function(req, res){
    let data = req.body;
    let aktivnost = [];
    if(data['grupa']!==undefined){
        aktivnost.push(db.Aktivnost.create({naziv: `${data['naziv']}`, pocetak: `${data['pocetak']}`, kraj: `${data['kraj']}`, predmetId: `${data['predmet']}`, grupaId: `${data['grupa']}`, danId: `${data['dan']}`, tipId: `${data['tip']}` }));
    }
    else{
        aktivnost.push(db.Aktivnost.create({naziv: `${data['naziv']}`, pocetak: `${data['pocetak']}`, kraj: `${data['kraj']}`, predmetId: `${data['predmet']}`, danId: `${data['dan']}`, tipId: `${data['tip']}` }));
    }
    Promise.all(aktivnost).then(res.end("Uspjesno ste dodali novu aktivnost")).catch(function(err){console.log(err);});
});
app.get("/v2/aktivnost", function(req, res){
    db.Aktivnost.findAll().then(function(aktivnost){
        res.end(JSON.stringify(aktivnost));
    });
});
app.delete("/v2/aktivnost", function(req, res){
    db.Aktivnost.findOne({where: {id: `${req.query.id}`}}).then(function(aktivnost){
        if(aktivnost!==null){
            aktivnost.destroy();
            res.end("Uspjesno ste obrisali aktivnost");
        }
        else{
            res.end("Aktivnost koju pokusavate da obrisete nije u tabeli");
        }
    });
});
app.put("/v2/aktivnost", function(req, res){
    let data = req.body;
    db.Aktivnost.findOne({where: {id: `${req.query.id}`}}).then(async function(aktivnost){
        if(aktivnost!==null){
            if(data['naziv']!==undefined){
                aktivnost.naziv = data['naziv'];
                await aktivnost.save();
            }
            if(data['pocetak']!==undefined){
                aktivnost.pocetak = data['pocetak'];
                await aktivnost.save();
            }
            if(data['kraj']!==undefined){
                aktivnost.kraj = data['kraj'];
                await aktivnost.save();
            }
            if(data['predmet']!==undefined){
                aktivnost.predmetId = data['predmet'];
                await aktivnost.save();
            }
            if(data['grupa']!==undefined){
                aktivnost.grupaId = data['grupa'];
                await aktivnost.save();
            }
            if(data['dan']!==undefined){
                aktivnost.danId = data['dan'];
                await aktivnost.save();
            }
            if(data['tip']!==undefined){
                aktivnost.tipId = data['tip'];
                await aktivnost.save();
            }
            res.end("Uspjesno ste update-ovali aktivnost");
        }
        else{
            res.end("Aktivnost koju pokusavate da update nije u tabeli");
        }
    });
});





app.post("/v2/dan", function(req, res){
    let data = req.body;
    let dan = [];
    dan.push(db.Dan.create({naziv: `${data['naziv']}`}));
    Promise.all(dan).then(res.end("Uspjesno ste dodali novi dan")).catch(function(err){console.log(err);});
});
app.get("/v2/dan", function(req, res){
    db.Dan.findAll().then(function(dan){
        res.end(JSON.stringify(dan));
    });
});
app.delete("/v2/dan", function(req, res){
    db.Dan.findOne({where: {id: `${req.query.id}`}}).then(function(dan){
        if(dan!==null){
            dan.destroy();
            res.end("Uspjesno ste obrisali dan");
        }
        else{
            res.end("Dan koji pokusavate da obrisete nije u tabeli");
        }
    });
});
app.put("/v2/dan", function(req, res){
    let data = req.body;
    db.Dan.findOne({where: {id: `${req.query.id}`}}).then(async function(dan){
        if(dan!==null){
            if(data['naziv']!==undefined){
                dan.naziv = data['naziv'];
                await dan.save();
            }
            res.end("Uspjesno ste izvrisili update dana");
        }
        else{
            res.end("Dan koji pokusavate da obrisete nije u tabeli");
        }
    });
});






app.post("/v2/tip", function(req, res){
    let data = req.body;
    let tip = [];
    tip.push(db.Tip.create({naziv: `${data['naziv']}`}));
    Promise.all(tip).then(res.end("Uspjesno ste dodali novi tip")).catch(function(err){console.log(err);});
});
app.get("/v2/tip", function(req, res){
    db.Tip.findAll().then(function(tip){
        res.end(JSON.stringify(tip));
    });
});
app.delete("/v2/tip", function(req, res){
    db.Tip.findOne({where: {id: `${req.query.id}`}}).then(function(tip){
        if(tip!==null){
            tip.destroy();
            res.end("Uspjesno ste obrisali tip");
        }
        else{
            res.end("Tip koji pokusavate da obrisete nije u tabeli");
        }
    });
});
app.put("/v2/tip", function(req, res){
    let data = req.body;
    db.Tip.findOne({where: {id: `${req.query.id}`}}).then(async function(tip){
        if(tip!==null){
            if(data['naziv']!==undefined){
                tip.naziv = data['naziv'];
                await tip.save();
            }
            res.end("Uspjesno ste updat-ovali tip");
        }
        else{
            res.end("Tip koji pokusavate da update nije u tabeli");
        }
    });
});








app.post("/v2/student", function(req, res){
    let data = req.body;
    let student = [];
    student.push(db.Student.create({ime: `${data['ime']}`, index: `${data['index']}`}));
    Promise.all(student).then(res.end("Uspjesno ste dodali novog studneta")).catch(function(err){console.log(err);});
});
app.get("/v2/student", function(req, res){
    db.Student.findAll().then(function(student){
        res.end(JSON.stringify(student));
    });
});
app.delete("/v2/student", function(req, res){
    db.Student.findOne({where: {id: `${req.query.id}`}}).then(function(student){
        if(student!==null){
            student.destroy();
            res.end("Uspjesno ste obrisali studenta");
        }
        else{
            res.end("Studenta kojeg pokusavate da obrisete nije u tabeli");
        }
    });
});
app.put("/v2/student", function(req, res){
    let data = req.body;
    db.Student.findOne({where: {id: `${req.query.id}`}}).then(async function(student){
        if(student!==null){
            if(data['ime']!==undefined){
                student.ime = data['ime'];
                await student.save();
            }
            
            if(data['index']!==undefined){
                student.index = data['index'];
                await student.save();
            }
            res.end("Uspjesno ste updat-ovali studenta");
        }
        else{
            res.end("Studenta kojeg pokusavate da update nije u tabeli");
        }
    });
});



app.put("/v2/user/:uid/grupa/:gid", function(req, res){
    let uid = req.params.uid;
    let gid = req.params.gid;
    const Grupa = db.Grupa;
    const Student = db.Student;
    Student.findByPk(uid).then(student=>{
        if(student===null){
            res.status(400);
            res.end("Greska pogresni podaci");
            return;
        }
        Grupa.findByPk(gid).then(grupa =>{
            if(grupa===null){
                res.status(400);
                res.end("Greska pogresni podaci");
                return;
            }
            try{
                student.addGrupaStudent(grupa,{ through: { grupa_student: false } });
                res.end("Uspjesno ste povezali studenta i grupu");
            }
            catch(err){
                res.end("Greska pogresni podaci");
            }
        });
    });
});


app.get("/v2/grupa/student/:id", function(req,res){
    let param = req.params.id;
    db.Student.findAll({
        include: "grupaStudent",
        where: {id: `${param}`}
    }).then(function(rezultat){
        res.end(JSON.stringify(rezultat[0].grupaStudent));
    });
});


app.put('/v2/student/:sid/grupa/:gid/novaGrupa/:ngid',function(req,res){
    let idStudent = req.params.sid;
    let idGrupa = req.params.gid;
    let novaGrupa = req.params.ngid;
    db.Grupa.findByPk(novaGrupa).then(function(grupa){
        if(grupa===null){
            res.status(400);
            res.end("Greska pogresni podaci");
            return;
        }
        db.Student.findOne({include: 'grupaStudent', where: {id: idStudent}}).then(function(student){
            let grupeSpojene = [];
            for (let i=0; i< student.grupaStudent.length; i++){
                if (student.grupaStudent[i].id != idGrupa){
                    grupeSpojene.push(student.grupaStudent[i]);
                }
            }    
            student.setGrupaStudent(grupa, {through: { grupa_student: false }});
            for (let i=0; i<grupeSpojene.length; i++){
                db.Grupa.findByPk(grupeSpojene[i].id).then(function(grupa){
                    student.addGrupaStudent(grupa, {through: { grupa_student: false }});
                });
            }
            res.end("Uspjesno ste promijenili grupu za predmet");
        });
    });
});


function dodajVezuIstiStudent(idS, idG, idNG){
    let idStudent = idS;
    let idGrupa = idG;
    let novaGrupa = idNG;
    db.Grupa.findByPk(novaGrupa).then(function(grupa){
        db.Student.findOne({include: 'grupaStudent', where: {id: idStudent}}).then(function(student){
            let grupeSpojene = [];
            for (let i=0; i< student.grupaStudent.length; i++){
                if (student.grupaStudent[i].id != idGrupa){
                    grupeSpojene.push(student.grupaStudent[i]);
                }
            }    
            student.setGrupaStudent(grupa, {through: { grupa_student: false }});
            for (let i=0; i<grupeSpojene.length; i++){
                db.Grupa.findByPk(grupeSpojene[i].id).then(function(grupa){
                    student.addGrupaStudent(grupa, {through: { grupa_student: false }});
                });
            }
        });
    });
}


function dodajVezuObicno(idS, idG){
    let uid = idS;
    let gid = idG;
    const Grupa = db.Grupa;
    const Student = db.Student;
    Student.findByPk(uid).then(student=>{
        Grupa.findByPk(gid).then(grupa =>{
            try{
                student.addGrupaStudent(grupa,{ through: { grupa_student: false } });
            }
            catch(err){
            }
        });
    });
}






app.post("/v2/dodajStudente/:gid",async function(req,res){
    let idGrupa = req.params.gid;
    let cijeliBody = req.body;
    let vrTextAreaPoRedovima = [];
    for(let i=0; i<cijeliBody.length; i++){
        vrTextAreaPoRedovima.push(cijeliBody[i].naziv);
    }
    let lista = [];
    for(let i=0; i<vrTextAreaPoRedovima.length; i++){
        await db.Student.findAll().then(async listaStudenata=>{
        let splitPoZarezu = vrTextAreaPoRedovima[i].split(",");
        let postoji = false;
        for(let j=0; j<listaStudenata.length; j++){
            if(listaStudenata[j].index===splitPoZarezu[1].trim()){
                if(listaStudenata[j].ime===splitPoZarezu[0].trim()){
                    await db.Student.findAll({include: "grupaStudent",where: {id: `${listaStudenata[j].id}`}}).then(async studPred=>{
                        studPred = studPred[0].grupaStudent;
                        await db.Grupa.findOne({where: {id: `${idGrupa}`}}).then(data=>{
                        studPred = JSON.stringify(studPred);
                        data = JSON.stringify(data);
                        let studentPredavanje = JSON.parse(studPred);
                        let selektIzListe= JSON.parse(data);
                        let postojis = false;
                        for(let i=0; i<studentPredavanje.length; i++){
                            if(studentPredavanje[i].predmetId===selektIzListe.predmetId){
                                dodajVezuIstiStudent(listaStudenata[j].id,studentPredavanje[i].id,idGrupa);
                                postojis = true;
                            }
                        }
                        if(postojis===false){
                            dodajVezuObicno(listaStudenata[j].id, idGrupa);
                        }
                        });
                        });
                        postoji=true;
                    }
                    else{
                        let obj = {naziv :`Student ${splitPoZarezu[0].trim()} nije kreiran jer postoji student ${listaStudenata[j].ime} sa istim indexom ${listaStudenata[j].index} \n`};
                        lista.push(obj);
                        postoji = true;
                    }

                }
            }

            if(postoji===false){
                await db.Student.create({ime: `${splitPoZarezu[0].trim()}`, index: `${splitPoZarezu[1].trim()}`}).then(rez=>{
                    db.Student.findAll().then(sviStudenti=>{
                        for(let i=0; i<sviStudenti.length; i++){
                            if(sviStudenti[i].ime===splitPoZarezu[0].trim() && sviStudenti[i].index===splitPoZarezu[1].trim()){
                                dodajVezuObicno(sviStudenti[i].id, idGrupa);
                            }
                        }
                    });
                });
            }
        }).then(re=>{
            if(i===vrTextAreaPoRedovima.length-1){
                if(lista.length===0){
                    let obj ={naziv :"status 200"};
                    lista.push(obj);
                    res.writeHead(200,{
                        'Content-Type': 'application/json'
                    });
                    res.end(JSON.stringify(lista));
                }
                else{
                    res.writeHead(200,{
                        'Content-Type': 'application/json'
                    });
                    res.end(JSON.stringify(lista));
                }
            }
        });
    }
});



app.listen(8080);