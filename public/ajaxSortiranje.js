function kreiranjeTabele(data, atribut){
    let rezultatTabela = "<table> \n" + 
    "<tr>\n";
    if(atribut === "Anaziv"){
        rezultatTabela = rezultatTabela + "<th onclick=\"sortirajPoNazivu()\">Naziv &#8595;</th> \n" +
        "<th onclick=\"sortirajPoAktivnosti()\">Aktivnost</th> \n" +
        "<th onclick=\"sortirajPoDanu()\">Dan</th> \n" +
        "<th onclick=\"sortirajPoPocetku()\">Početak</th> \n" +
        "<th onclick=\"sortirajPoKraju()\">Kraj</th> \n" +
        "</tr> \n";
    }
    else if(atribut === "Dnaziv"){
        rezultatTabela = rezultatTabela + "<th onclick=\"sortirajPoNazivu()\">Naziv &#8593;</th> \n" +
        "<th onclick=\"sortirajPoAktivnosti()\">Aktivnost</th> \n" +
        "<th onclick=\"sortirajPoDanu()\">Dan</th> \n" +
        "<th onclick=\"sortirajPoPocetku()\">Početak</th> \n" +
        "<th onclick=\"sortirajPoKraju()\">Kraj</th> \n" +
        "</tr> \n";
    }
    else if(atribut === "Aaktivnost"){
        rezultatTabela = rezultatTabela + "<th onclick=\"sortirajPoNazivu()\">Naziv</th> \n" +
        "<th onclick=\"sortirajPoAktivnosti()\">Aktivnost &#8595;</th> \n" +
        "<th onclick=\"sortirajPoDanu()\">Dan</th> \n" +
        "<th onclick=\"sortirajPoPocetku()\">Početak</th> \n" +
        "<th onclick=\"sortirajPoKraju()\">Kraj</th> \n" +
        "</tr> \n";
    }
    else if(atribut === "Daktivnost"){
        rezultatTabela = rezultatTabela + "<th onclick=\"sortirajPoNazivu()\">Naziv</th> \n" +
        "<th onclick=\"sortirajPoAktivnosti()\">Aktivnost &#8593;</th> \n" +
        "<th onclick=\"sortirajPoDanu()\">Dan</th> \n" +
        "<th onclick=\"sortirajPoPocetku()\">Početak</th> \n" +
        "<th onclick=\"sortirajPoKraju()\">Kraj</th> \n" +
        "</tr> \n";
    }
    else if(atribut === "Adan"){
        rezultatTabela = rezultatTabela + "<th onclick=\"sortirajPoNazivu()\">Naziv</th> \n" +
        "<th onclick=\"sortirajPoAktivnosti()\">Aktivnost</th> \n" +
        "<th onclick=\"sortirajPoDanu()\">Dan &#8595</th> \n" +
        "<th onclick=\"sortirajPoPocetku()\">Početak</th> \n" +
        "<th onclick=\"sortirajPoKraju()\">Kraj</th> \n" +
        "</tr> \n";
    }
    else if(atribut === "Ddan"){
        rezultatTabela = rezultatTabela + "<th onclick=\"sortirajPoNazivu()\">Naziv</th> \n" +
        "<th onclick=\"sortirajPoAktivnosti()\">Aktivnost</th> \n" +
        "<th onclick=\"sortirajPoDanu()\">Dan &#8593</th> \n" +
        "<th onclick=\"sortirajPoPocetku()\">Početak</th> \n" +
        "<th onclick=\"sortirajPoKraju()\">Kraj</th> \n" +
        "</tr> \n";
    }
    else if(atribut === "Apocetak"){
        rezultatTabela = rezultatTabela + "<th onclick=\"sortirajPoNazivu()\">Naziv</th> \n" +
        "<th onclick=\"sortirajPoAktivnosti()\">Aktivnost</th> \n" +
        "<th onclick=\"sortirajPoDanu()\">Dan</th> \n" +
        "<th onclick=\"sortirajPoPocetku()\">Početak &#8595</th> \n" +
        "<th onclick=\"sortirajPoKraju()\">Kraj</th> \n" +
        "</tr> \n";
    }
    else if(atribut === "Dpocetak"){
        rezultatTabela = rezultatTabela + "<th onclick=\"sortirajPoNazivu()\">Naziv</th> \n" +
        "<th onclick=\"sortirajPoAktivnosti()\">Aktivnost</th> \n" +
        "<th onclick=\"sortirajPoDanu()\">Dan</th> \n" +
        "<th onclick=\"sortirajPoPocetku()\">Početak &#8593</th> \n" +
        "<th onclick=\"sortirajPoKraju()\">Kraj</th> \n" +
        "</tr> \n";
    }
    else if(atribut === "Akraj"){
        rezultatTabela = rezultatTabela + "<th onclick=\"sortirajPoNazivu()\">Naziv</th> \n" +
        "<th onclick=\"sortirajPoAktivnosti()\">Aktivnost</th> \n" +
        "<th onclick=\"sortirajPoDanu()\">Dan</th> \n" +
        "<th onclick=\"sortirajPoPocetku()\">Početak</th> \n" +
        "<th onclick=\"sortirajPoKraju()\">Kraj &#8595</th> \n" +
        "</tr> \n";
    }
    else if(atribut === "Dkraj"){
        rezultatTabela = rezultatTabela + "<th onclick=\"sortirajPoNazivu()\">Naziv</th> \n" +
        "<th onclick=\"sortirajPoAktivnosti()\">Aktivnost</th> \n" +
        "<th onclick=\"sortirajPoDanu()\">Dan</th> \n" +
        "<th onclick=\"sortirajPoPocetku()\">Početak</th> \n" +
        "<th onclick=\"sortirajPoKraju()\">Kraj &#8593</th> \n" +
        "</tr> \n";
    }
    else {
        rezultatTabela = rezultatTabela + "<th onclick=\"sortirajPoNazivu()\">Naziv</th> \n" +
        "<th onclick=\"sortirajPoAktivnosti()\">Aktivnost</th> \n" +
        "<th onclick=\"sortirajPoDanu()\">Dan</th> \n" +
        "<th onclick=\"sortirajPoPocetku()\">Početak</th> \n" +
        "<th onclick=\"sortirajPoKraju()\">Kraj</th> \n" +
        "</tr> \n";
    }
    for(let i=0; i<data.length; i++){
        rezultatTabela = rezultatTabela + "<tr> \n" +
        `<td>${data[i].naziv}</td> \n` +
        `<td>${data[i].aktivnost}</td> \n` +
        `<td>${data[i].dan}</td> \n` +
        `<td>${data[i].pocetak}</td> \n` +
        `<td>${data[i].kraj}</td> \n` +
        "</tr> \n";
    }
    rezultatTabela = rezultatTabela + "</table>";
    return rezultatTabela;
}

function ucitajSortirano(dan,atribut,callback){
    var ajax = new XMLHttpRequest(); 
    ajax.onreadystatechange = function() {
    if (ajax.readyState == 4 && ajax.status == 200) 
        document.getElementById("demo").innerHTML = kreiranjeTabele(JSON.parse(ajax.responseText),atribut); 
        callback(ajax.responseText,null);
    if (ajax.readyState == 4 && ajax.status == 404) 
        document.getElementById("demo").innerHTML = "Greska: nepoznat URL"; 
        callback(null,"Greška: Error 404 prilikom učitavanja stranice");
    }
    if(dan===null || dan===""){  
        ajax.open("GET",`v1/raspored?sort=${atribut}`,true);
    }
    else if(atribut===null || atribut===""){
        ajax.open("GET",`v1/raspored?dan=${dan}`,true);
    }
    else if ((dan===null || dan==="") && (atribut===null || atribut==="")){
        ajax.open("GET",`v1/raspored`,true);
    }
    else{
        ajax.open("GET",`v1/raspored?sort=${atribut}&dan=${dan}`,true);
    }
    ajax.send();
}