const btn1 = document.querySelector('#btnzatrenutni');
btn1.onclick = function () {
const rbs = document.querySelectorAll('input[name="izbor"]');
let selectedValue;
    for (const rb of rbs) {
        if (rb.checked) {
            selectedValue = rb.value;
            break;
        }
    }
    let rasp = new Raspored(`
    BWT-grupa2,vjezbe,ponedjeljak,13:30,15:00
    BWT,predavanje,ponedjeljak,15:00,18:00
    MUR1,predavanje,utorak,09:00,11:00
    MUR1-grupa1,vjezbe,srijeda,11:00,12:30
    MUR1-grupa2,vjezbe,srijeda,12:30,14:00
    RMA,predavanje,ponedjeljak,09:00,12:00
    BWT-grupa1,vjezbe,ponedjeljak,12:00,13:30
    FWT-grupa2,vjezbe,srijeda,11:00,12:30
    FWT-grupa1,vjezbe,srijeda,12:30,14:00
    ASP,predavanje,srijeda,09:00,12:00
    MUR2,predavanje,cetvrtak,12:00,15:00
    FWT,predavanje,cetvrtak,09:00,10:30
    `);
    if(selectedValue==="Prvi string"){
        alert(rasp.dajTrenutnuAktivnost("02-11-2020T08:30:00","grupa2"));
    }
    else if(selectedValue==="Drugi string"){
        alert(rasp.dajTrenutnuAktivnost("03-11-2020T09:30:00","grupa1"));
    }
    else if(selectedValue==="Treci string"){
        alert(rasp.dajTrenutnuAktivnost("10-10-2020T12:00:00","grupa1"));
    }
    else if(selectedValue==="Cetvrti string"){
        alert(rasp.dajTrenutnuAktivnost("02-11-2020T11:00:00","grupa2"));
    }
    else if(selectedValue==="Peti string"){
        alert(rasp.dajTrenutnuAktivnost("02-11-2020T19:30:00","grupa2"));
    }
};

const btn2 = document.querySelector('#btnzasljedeci');
btn2.onclick = function () {
const rbs = document.querySelectorAll('input[name="izbor"]');
let selectedValue;
    for (const rb of rbs) {
        if (rb.checked) {
            selectedValue = rb.value;
            break;
        }
    }
    let rasp = new Raspored(`
    BWT-grupa2,vjezbe,ponedjeljak,13:30,15:00
    BWT,predavanje,ponedjeljak,15:00,18:00
    MUR1,predavanje,utorak,09:00,11:00
    MUR1-grupa1,vjezbe,srijeda,11:00,12:30
    MUR1-grupa2,vjezbe,srijeda,12:30,14:00
    RMA,predavanje,ponedjeljak,09:00,12:00
    BWT-grupa1,vjezbe,ponedjeljak,12:00,13:30
    FWT-grupa2,vjezbe,srijeda,11:00,12:30
    FWT-grupa1,vjezbe,srijeda,12:30,14:00
    ASP,predavanje,srijeda,09:00,12:00
    MUR2,predavanje,cetvrtak,12:00,15:00
    FWT,predavanje,cetvrtak,09:00,10:30
    `);
    if(selectedValue==="Prvi string"){
        alert(rasp.dajSljedecuAktivnost("02-11-2020T08:30:00","grupa2"));
    }
    else if(selectedValue==="Drugi string"){
        alert(rasp.dajSljedecuAktivnost("03-11-2020T09:30:00","grupa1"));
    }
    else if(selectedValue==="Treci string"){
        alert(rasp.dajSljedecuAktivnost("10-10-2020T12:00:00","grupa1"));
    }
    else if(selectedValue==="Cetvrti string"){
        alert(rasp.dajSljedecuAktivnost("02-11-2020T11:00:00","grupa2"));
    }
    else if(selectedValue==="Peti string"){
        alert(rasp.dajSljedecuAktivnost("02-11-2020T19:30:00","grupa2"));
    }
};

const btn3 = document.querySelector('#btnzaprethodni');
btn3.onclick = function () {
const rbs = document.querySelectorAll('input[name="izbor"]');
let selectedValue;
    for (const rb of rbs) {
        if (rb.checked) {
            selectedValue = rb.value;
            break;
        }
    }
    let rasp = new Raspored(`
    BWT-grupa2,vjezbe,ponedjeljak,13:30,15:00
    BWT,predavanje,ponedjeljak,15:00,18:00
    MUR1,predavanje,utorak,09:00,11:00
    MUR1-grupa1,vjezbe,srijeda,11:00,12:30
    MUR1-grupa2,vjezbe,srijeda,12:30,14:00
    RMA,predavanje,ponedjeljak,09:00,12:00
    BWT-grupa1,vjezbe,ponedjeljak,12:00,13:30
    FWT-grupa2,vjezbe,srijeda,11:00,12:30
    FWT-grupa1,vjezbe,srijeda,12:30,14:00
    ASP,predavanje,srijeda,09:00,12:00
    MUR2,predavanje,cetvrtak,12:00,15:00
    FWT,predavanje,cetvrtak,09:00,10:30
    `);
    if(selectedValue==="Prvi string"){
        alert(rasp.dajPrethodnuAktivnost("02-11-2020T08:30:00","grupa2"));
    }
    else if(selectedValue==="Drugi string"){
        alert(rasp.dajPrethodnuAktivnost("03-11-2020T09:30:00","grupa1"));
    }
    else if(selectedValue==="Treci string"){
        alert(rasp.dajPrethodnuAktivnost("10-10-2020T12:00:00","grupa1"));
    }
    else if(selectedValue==="Cetvrti string"){
        alert(rasp.dajPrethodnuAktivnost("02-11-2020T11:00:00","grupa2"));
    }
    else if(selectedValue==="Peti string"){
        alert(rasp.dajPrethodnuAktivnost("02-11-2020T19:30:00","grupa2"));
    }
};