/*Greška kada nema nemamo ID "section-*"" pod ovim nayivom vec smo stavili neki drugi naziv u našem slučaju 
"nekiDrugiID" naravno neki testovi prolaze jer je default vrijednost koja se vraća null*/
class GoogleMeet{
    static dajZadnjePredavanje(ulaz){
        let parsiranje = new DOMParser();
        let document = parsiranje.parseFromString(ulaz, "text/html");
        let listUl = document.getElementsByTagName("UL");
        let strigRezultat = null;
        let prClass = document.getElementsByClassName("course-content");
        if(prClass.length === 0) return strigRezultat;
        let pamtiId = null;
        for(let i=0; i<listUl.length; i++){
            if(listUl[i].className.includes("weeks")){
                var lista = listUl[i].getElementsByTagName("LI");
                for(let j=0; j<lista.length; j++){
                    if(lista[j].id.includes("nekiDrugiID")){
                        let listaNodees = lista[j].getElementsByTagName("A");
                        for(let z=0; z<listaNodees.length; z++){
                            if(listaNodees[z].length!==0 && listaNodees[z].textContent.includes("predavanj")
                            && listaNodees[z].getAttribute('href').includes('meet.google.com')){
                                if(pamtiId === null){
                                    pamtiId = listaNodees[z].id;
                                    strigRezultat = listaNodees[z].href;
                                }
                                else if((pamtiId.localeCompare(listaNodees[z].id.toString))===-1){
                                    pamtiId = listaNodees[z].id;
                                    strigRezultat = listaNodees[z].getAttribute("href");
                                }
                            }
                        }
                    }
                }
            }
        }
        return strigRezultat;
    }
    static dajZadnjuVježbu(ulaz){
        let parsiranje = new DOMParser();
        let document = parsiranje.parseFromString(ulaz, "text/html");
        let listUl = document.getElementsByTagName("UL");
        let strigRezultat = null;
        let prClass = document.getElementsByClassName("course-content");
        if(prClass.length === 0) return strigRezultat;
        let pamtiId = null;
        for(let i=0; i<listUl.length; i++){
            if(listUl[i].className.includes("weeks")){
                var lista = listUl[i].getElementsByTagName("LI");
                for(let j=0; j<lista.length; j++){
                    if(lista[j].id.includes("nekiDrugiID")){
                        let listaNodees = lista[j].getElementsByTagName("A");
                        for(let z=0; z<listaNodees.length; z++){
                            if(listaNodees[z].length!==0 && listaNodees[z].getAttribute('href').includes('meet.google.com')){
                                if(listaNodees[z].textContent.includes("vjezb") || listaNodees[z].textContent.includes("vježb")){
                                    if(pamtiId === null){
                                        pamtiId = listaNodees[z].id;
                                        strigRezultat = listaNodees[z].href;
                                    }
                                    else if((pamtiId.localeCompare(listaNodees[z].id.toString))===-1){
                                        pamtiId = listaNodees[z].id;
                                        strigRezultat = listaNodees[z].getAttribute("href");
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return strigRezultat;
    }
}