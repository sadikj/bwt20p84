const btn1 = document.querySelector('#prvo');
btn1.onclick = function () {  
    document.getElementById("zatextbox").innerHTML = primjeri[0];
};

const btn2 = document.querySelector('#drugo');
btn2.onclick = function () { 
    document.getElementById("zatextbox").innerHTML = primjeri[1];
};

const btn3 = document.querySelector('#trece');
btn3.onclick = function () { 
    document.getElementById("zatextbox").innerHTML = primjeri[2];
};

const btn4 = document.querySelector('#cetvrto');
btn4.onclick = function () { 
    document.getElementById("zatextbox").innerHTML = primjeri[3];
};

const btn5 = document.querySelector('#peto');
btn5.onclick = function () { 
    document.getElementById("zatextbox").innerHTML = primjeri[4];
};

const btn6 = document.querySelector('#btnzapredavanje');
btn6.onclick = function () {
const rbs = document.querySelectorAll('input[name="izbor"]');
let selectedValue;
    for (const rb of rbs) {
        if (rb.checked) {
            selectedValue = rb.value;
            break;
        }
    }
    if(selectedValue==="Prvi string"){
        alert(GoogleMeet.dajZadnjePredavanje(primjeri[0]));
    }
    else if(selectedValue==="Drugi string"){
        alert(GoogleMeet.dajZadnjePredavanje(primjeri[1]));
    }
    else if(selectedValue==="Treci string"){
        alert(GoogleMeet.dajZadnjePredavanje(primjeri[2]));
    }
    else if(selectedValue==="Cetvrti string"){
        alert(GoogleMeet.dajZadnjePredavanje(primjeri[3]));
    }
    else if(selectedValue==="Peti string"){
        alert(GoogleMeet.dajZadnjePredavanje(primjeri[4]));
    }
};

const btn7 = document.querySelector('#btnzavjezbe');
btn7.onclick = function () {
const rbs = document.querySelectorAll('input[name="izbor"]');
let selectedValue;
    for (const rb of rbs) {
        if (rb.checked) {
            selectedValue = rb.value;
            break;
        }
    }
    if(selectedValue==="Prvi string"){
        alert(GoogleMeet.dajZadnjuVježbu(primjeri[0]));
    }
    else if(selectedValue==="Drugi string"){
        alert(GoogleMeet.dajZadnjuVježbu(primjeri[1]));
    }
    else if(selectedValue==="Treci string"){
        alert(GoogleMeet.dajZadnjuVježbu(primjeri[2]));
    }
    else if(selectedValue==="Cetvrti string"){
        alert(GoogleMeet.dajZadnjuVježbu(primjeri[3]));
    }
    else if(selectedValue==="Peti string"){
        alert(GoogleMeet.dajZadnjuVježbu(primjeri[4]));
    }
};