let assert = chai.assert; 
describe('Zadatak 2', function() { 
 describe('#test googlemeet classe', function() { 
   it('Test kada nema niti jednog predavanja', function() {
       assert.equal(GoogleMeet.dajZadnjePredavanje(primjeri[4]), null); 
   }); 
   it('Test kada nema niti jedne vježbe', function() {
       assert.equal(GoogleMeet.dajZadnjuVježbu(primjeri[0]), null); 
   }); 
   it('Test kada je string prazan', function() {
       assert.equal(GoogleMeet.dajZadnjePredavanje(primjeri[5]), null);
       assert.equal(GoogleMeet.dajZadnjuVježbu(primjeri[5]), null);
   });
   it('Test kada svaka druga sedmica ima vježbu', function() {
       assert.equal(GoogleMeet.dajZadnjuVježbu(primjeri[6]), "http://meet.google.com/maf-fpnx-ggg");
    });
    it('Test kada imaju linkovi sa ispravnim nazivima ali url ne sadrži meet.google.com', function() {
        assert.equal(GoogleMeet.dajZadnjePredavanje(primjeri[7]), null);
        assert.equal(GoogleMeet.dajZadnjuVježbu(primjeri[7]), null);
    });
    it('Test kada postoje linkovi sa ispravnim url-om ali ne sadrže tekst vjezb, vježb i predavanj', function() {
        assert.equal(GoogleMeet.dajZadnjePredavanje(primjeri[8]), null);
        assert.equal(GoogleMeet.dajZadnjuVježbu(primjeri[8]), null);
    });
    it('Test kada se link nalazi van ul liste', function() {
        assert.equal(GoogleMeet.dajZadnjePredavanje(primjeri[9]), "http://meet.google.com/xqj-rvuh-hgd");
        assert.equal(GoogleMeet.dajZadnjuVježbu(primjeri[9]), "http://meet.google.com/maf-fpnx-bdt");
    });
    it('Test kada su predavanja i vježbe samo u prvoj sedmici', function() {
        assert.equal(GoogleMeet.dajZadnjePredavanje(primjeri[9]), "http://meet.google.com/xqj-rvuh-hgd");
        assert.equal(GoogleMeet.dajZadnjuVježbu(primjeri[9]), "http://meet.google.com/maf-fpnx-bdt");
    });
    it('Test kada nije validan html kod - kada ne sadrži div ‘course-content’ sa ul listom ‘weeks’ i elementima liste ‘section-*’', function() {
        assert.equal(GoogleMeet.dajZadnjePredavanje(primjeri[10]), null);
        assert.equal(GoogleMeet.dajZadnjuVježbu(primjeri[10]), null);
    });  
  });  
}); 