window.onload =  function (){
    posaljiZahtjevPredmet();
    posaljiZahtjevAktivnost();
};

let provjeriPredmet = false;
let provjeriAktivnost = false; 

function napraviTabeluPredmeta(data){
    let rezultatTabela = "<table> \n" + "<tr>\n";
    rezultatTabela = rezultatTabela + "<th>Naziv predmeta</th> \n" +"</tr> \n";
    for(let i=0; i<data.length; i++){
        rezultatTabela = rezultatTabela + "<tr> \n" +
        `<td>${data[i].naziv}</td> \n` +
        "</tr> \n";
    }
    rezultatTabela = rezultatTabela + "</table>";
    return rezultatTabela;
}
function napraviTabeluAktivnosti(data){
    let rezultatTabela = "<table> \n" + "<tr>\n";
    rezultatTabela = rezultatTabela + "<th>Naziv</th> \n" +
    "<th>Aktivnost</th> \n" +
    "<th>Dan</th> \n" +
    "<th>Pocetak</th> \n" +
    "<th>Kraj</th> \n" +
    "</tr> \n";
    for(let i=0; i<data.length; i++){
        rezultatTabela = rezultatTabela + "<tr> \n" +
        `<td>${data[i].naziv}</td> \n` +
        `<td>${data[i].aktivnost}</td> \n` +
        `<td>${data[i].dan}</td> \n` +
        `<td>${data[i].pocetak}</td> \n` +
        `<td>${data[i].kraj}</td> \n` +
        "</tr> \n";
    }
    rezultatTabela = rezultatTabela + "</table>";
    return rezultatTabela;
}
function posaljiZahtjevPredmet(){
    var ajax = new XMLHttpRequest(); 
    ajax.onreadystatechange = function() {
    if (ajax.readyState == 4 && ajax.status == 200) 
        document.getElementById("predmeti").innerHTML = napraviTabeluPredmeta(JSON.parse(ajax.responseText)); 
    if (ajax.readyState == 4 && ajax.status == 404) 
        document.getElementById("predmeti").innerHTML = "Greska: nepoznat URL"; 
    }
    ajax.open("GET",`http://localhost:8080/v1/predmet`,true);
    ajax.send();
} 
function posaljiZahtjevAktivnost(){
    var ajax = new XMLHttpRequest(); 
    ajax.onreadystatechange = function() {
    if (ajax.readyState == 4 && ajax.status == 200) 
        document.getElementById("aktivnosti").innerHTML = napraviTabeluAktivnosti(JSON.parse(ajax.responseText)); 
    if (ajax.readyState == 4 && ajax.status == 404) 
        document.getElementById("aktivnosti").innerHTML = "Greska: nepoznat URL"; 
    }
    ajax.open("GET",`http://localhost:8080/v1/raspored`,true);
    ajax.send();
}

function posaljiPredmet(data){
    var ajax = new XMLHttpRequest();
    let split;
    if(data.includes("-")){
        split = data.split("-");
        ajax.open("POST", "http://localhost:8080/v1/predmet", true);
	    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        let string = "nazivpredmeta=" + split[0];
        ajax.send(string);
    }
    else{
	    ajax.open("POST", "http://localhost:8080/v1/predmet", true);
	    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        let string = "nazivpredmeta=" + data;
        ajax.send(string);
    }
}

async function obrisiPredmet(nazivpredmeta){
    let res = await fetch('http://localhost:8080/v1/raspored');
    let data = await res.text();
    if(data.includes(nazivpredmeta) === false){
        let novi = await fetch(`http://localhost:8080/v1/predmet?naziv=` + nazivpredmeta, {    
        method: 'DELETE'
        }).then(posaljiZahtjevPredmet());
    }
}

function posaljiAktivnost(nazivpredmeta, aktivnost, dan, vrijemepocetka, vrijemekraja){
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function() {
        if (ajax.readyState == 4 && ajax.status == 200) 
            if(ajax.responseText === `Uspješno je dodana aktivnost.`){
                document.getElementById("poruka").innerHTML = ajax.responseText;
            }
            else if(ajax.responseText === `Aktivnost nije dodana, pokušajte ponovo.`){
                if(nazivpredmeta.includes("-")){
                    let split = nazivpredmeta.split("-");
                    obrisiPredmet(split[0]);
                }
                else{
                    obrisiPredmet(nazivpredmeta);
                }
                document.getElementById("poruka").innerHTML = ajax.responseText;
            }
        if (ajax.readyState == 4 && ajax.status == 404) 
            document.getElementById("aktivnosti").innerHTML = "Greska: nepoznat URL"; 
    }
	ajax.open("POST", "http://localhost:8080/v1/raspored", true);
	ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	let string = "nazivpredmeta=" + nazivpredmeta + "&aktivnost=" + aktivnost + "&dan=" + dan + "&vrijemepocetka=" + vrijemepocetka + "&vrijemekraja=" + vrijemekraja;
    ajax.send(string);
}

function posaljiSveStoTreba(){
    posaljiPredmet(document.forms["form"]["nazivpredmeta"].value);
    posaljiAktivnost(document.forms["form"]["nazivpredmeta"].value, document.forms["form"]["aktivnost"].value, document.forms["form"]["dan"].value, document.forms["form"]["vrijemepocetka"].value, document.forms["form"]["vrijemekraja"].value);
    posaljiZahtjevPredmet();
    posaljiZahtjevAktivnost();
}

