window.onload = function() {
    ucitajSortirano("", "", ispisiRezultat);
};

function dohvatiValue(){
    var x = document.getElementById("selectBox").selectedIndex;
    let rezultat = document.getElementsByTagName("option")[x].value;
    ucitajSortirano(rezultat, "", ispisiRezultat);
    return rezultat;
}


function ispisiRezultat(data,error){ 
    if(!error)console.log(JSON.stringify(data)); 
} 

let sortNaziv = true;
let sortAktivnost = true;
let sortDan = true;
let sortPocetak = true;
let sortKraj = true;
function sortirajPoNazivu(){
    if(sortNaziv===true){
        ucitajSortirano(dohvatiValue(),"Anaziv",ispisiRezultat);
        sortNaziv=false; sortAktivnost = true; sortDan = true;
        sortPocetak = true; sortKraj = true;
    }
    else{
        ucitajSortirano(dohvatiValue(),"Dnaziv",ispisiRezultat);
        sortNaziv=true; sortAktivnost = true; sortDan = true;
        sortPocetak = true; sortKraj = true;
    }
}
function sortirajPoAktivnosti(){
    if(sortAktivnost===true){
        ucitajSortirano(dohvatiValue(),"Aaktivnost",ispisiRezultat);
        sortAktivnost=false; sortNaziv = true; sortDan = true;
        sortPocetak = true; sortKraj = true;
    }
    else{
        ucitajSortirano(dohvatiValue(),"Daktivnost",ispisiRezultat);
        sortAktivnost=true; sortNaziv = true; sortDan = true;
        sortPocetak = true; sortKraj = true;
    }
}
function sortirajPoDanu(){
    if(sortDan===true){
        ucitajSortirano(dohvatiValue(),"Adan",ispisiRezultat);
        sortDan=false; sortNaziv = true; sortAktivnost=true;
        sortPocetak = true; sortKraj = true;
    }
    else{
        ucitajSortirano(dohvatiValue(),"Ddan",ispisiRezultat);
        sortDan=true; sortNaziv = true; sortAktivnost=true;
        sortPocetak = true; sortKraj = true;
    }
}
function sortirajPoPocetku(){
    if(sortPocetak===true){
        ucitajSortirano(dohvatiValue(),"Apocetak",ispisiRezultat);
        sortPocetak=false; sortAktivnost = true; sortDan = true;
        sortNaziv = true; sortKraj=true;
    }
    else{
        ucitajSortirano(dohvatiValue(),"Dpocetak",ispisiRezultat);
        sortPocetak=true; sortAktivnost = true; sortDan = true;
        sortNaziv = true; sortKraj=true;
    }
}
function sortirajPoKraju(){
    if(sortKraj===true){
        ucitajSortirano(dohvatiValue(),"Akraj",ispisiRezultat);
        sortKraj=false; sortAktivnost = true; sortDan = true;
        sortNaziv = true; sortPocetak=true;
    }
    else{
        ucitajSortirano(dohvatiValue(),"Dkraj",ispisiRezultat);
        sortKraj=true; sortAktivnost = true; sortDan = true;
        sortNaziv = true; sortPocetak=true;
    }
}