let assert = chai.assert; 
describe('Zadatak 4', function() { 
 describe('#test raspored classe', function() { 
   it('dajTrenutnuAktivnost kada nema trenutne aktivnosti u datom vremenu', function() {
       let novi = new Raspored(raspored);
       assert.equal(novi.dajTrenutnuAktivnost("02-11-2020T19:00:00","grupa1"), "Trenutno nema aktivnosti"); 
   });
   it('dajTrenutnuAktivnost na početku neke aktivnosti', function() {
        let novi = new Raspored(raspored);
        assert.equal(novi.dajTrenutnuAktivnost("02-11-2020T13:30:00","grupa2"), "BWT 90"); 
    });
    it('dajTrenutnuAktivnost na kraju neke aktivnosti', function() {
        let novi = new Raspored(raspored);
        assert.equal(novi.dajTrenutnuAktivnost("02-11-2020T18:00:00","grupa2"), "Trenutno nema aktivnosti"); 
    });
    it('dajTrenutnuAktivnost kada postoji vježba u datom vremenu ali nije ispravna grupa', function() {
        let novi = new Raspored(raspored);
        assert.equal(novi.dajTrenutnuAktivnost("02-11-2020T14:00:00","grupa1"), "Trenutno nema aktivnosti"); 
    }); 
    it('dajTrenutnuAktivnost kada postoji vježba u datom vremenu i ispravna je grupa', function() {
        let novi = new Raspored(raspored);
        assert.equal(novi.dajTrenutnuAktivnost("04-11-2020T13:00:00","grupa1"), "FWT 60"); 
    }); 
    it('dajPrethodnuAktivnost za slučaj kada je prva prethodna aktivnost vježba sa pogrešnom grupom, tada trebate vratiti aktivnost prije nje', function() {
        let novi = new Raspored(raspored);
        assert.equal(novi.dajPrethodnuAktivnost("02-11-2020T16:00:00","grupa1"), "BWT 150"); 
    });
    it('dajPrethodnuAktivnost prije prve aktivnosti u ponedjeljak, treba vratiti posljednju aktivnost iz petka, ili četvrtka ako nema aktivnosti u petak itd.', function() {
        let novi = new Raspored(raspored);
        assert.equal(novi.dajPrethodnuAktivnost("02-11-2020T08:00:00","grupa1"), "MUR2 5340"); 
    }); 
    it('dajSljedecuAktivnost za slučaj kada je prva prethodna aktivnost vježba sa pogrešnom grupom, tada trebate vratiti aktivnost prije nje', function() {
        let novi = new Raspored(raspored);
        assert.equal(novi.dajSljedecuAktivnost("02-11-2020T13:00:00","grupa1"), "BWT 120"); 
    }); 
    it('dajSljedecuAktivnost poslije posljednje aktivnosti u ponedjeljak, treba vratiti string "Nastava je gotova za danas"', function() {
        let novi = new Raspored(raspored);
        assert.equal(novi.dajSljedecuAktivnost("02-11-2020T22:00:00","grupa1"), "Nastava je gotova za danas"); 
    });
  });  
}); 