window.onload =  function (){
    ucitajListu();
};


function kreirajListu(lista){
    lista = JSON.parse(lista);
    let rezultat = "";
    for(let i=0; i<lista.length; i++){
        rezultat = rezultat + `<option value=${lista[i].id}>${lista[i].naziv}</option>` + "\n";
    }
    return rezultat;
}

function selektovan(){
    var x = document.getElementById("neki").selectedIndex;
    let rezultat = document.getElementsByTagName("option")[x].value;
    return rezultat;
}

function ucitajListu(){
    var ajax = new XMLHttpRequest(); 
    ajax.onreadystatechange = function() {
    if (ajax.readyState == 4 && ajax.status == 200) 
        document.getElementById("neki").innerHTML = kreirajListu(ajax.responseText);
    if (ajax.readyState == 4 && ajax.status == 404) 
        document.getElementById("neki").innerHTML = "Greska: nepoznat URL"; 
    } 
    ajax.open("GET",`v2/grupa`,true);
    ajax.send();
}

async function dohvatiStudente(){
    let res = await fetch('http://localhost:8080/v2/student');
    let data = await res.json();
    return await data;
}

function dodajStudente(){
    let vrijednostTextArea = document.getElementById('area').value;
    let vrTextAreaPoRedovima = vrijednostTextArea.toString().split("\n");
    let rezultat = [];
    for(let i=0; i<vrTextAreaPoRedovima.length; i++){
        let objekat={};
        objekat["naziv"] = vrTextAreaPoRedovima[i].trim();
        rezultat.push(objekat);
    }
    $.ajax({
        url: `/v2/dodajStudente/${selektovan()}`,
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            let stringFinal = "";
            for(let i=0; i<data.length; i++){
                stringFinal = stringFinal + data[i].naziv;
            }
            document.getElementById('area').value = stringFinal;
        },
        data: JSON.stringify(rezultat)
    });
}

function studentGrupa(){
    dodajStudente();
}
