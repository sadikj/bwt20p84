/*Nešto o grešci rečeno u komentarima ispod*/

class Raspored{
    constructor(ulaz){
        ulaz = ulaz.trim();
        let razdvojiPoZaRedu = ulaz.split("\n");
        let rezultat = [];
        let naslovi = ["naziv","aktivnost","dan","start","end"];
        for(let i=0; i<razdvojiPoZaRedu.length; i++){
            let razdvojiPoZarezu = razdvojiPoZaRedu[i].split(",");
            let objekat={};
            for(let j=0; j<razdvojiPoZarezu.length; j++){
                objekat[naslovi[j].trim()] = razdvojiPoZarezu[j].trim();
            }
            rezultat.push(objekat)
        }
        this.raspored = rezultat;
    }
    trenutniDan(stringDana){
        let dani = ["ponedjeljak", "utorak", "srijeda", "cetvrtak", "petak", "subota", "nedjelja"];
        let trenutniDan = null;
        for(let i=0; dani.length; i++){
            if(i==0 && stringDana===dani[i]) return 1;
            else if(i==1 && stringDana===dani[i]) return 2;
            else if(i==2 && stringDana===dani[i]) return 3;
            else if(i==3 && stringDana===dani[i]) return 4;
            else if(i==4 && stringDana===dani[i]) return 5;
            else if(i==5 && stringDana===dani[i]) return 6;
            else if(i==6 && stringDana===dani[i]) return 7;
        }
    }
    dajTrenutnuAktivnost(datumString, nazivGrupe){
        let razdvojiVrijeme = datumString.split('T');
        let raz = razdvojiVrijeme[0].split('-');
        let sms = razdvojiVrijeme[1].split(':');
        let rezultat = "Trenutno nema aktivnosti";
        let datum = new Date(raz[2].trim()+"-"+raz[1].trim()+"-"+raz[0].trim()+"T"+sms[0].trim()+":"+sms[1].trim()+":"+sms[2].trim());
        for(let i=0 ; i<this.raspored.length; i++){
            if(this.raspored[i].naziv.includes(nazivGrupe) || this.raspored[i].aktivnost==="predavanje"){
                let brojDan = this.trenutniDan(this.raspored[i].dan);
                if(brojDan===datum.getDay()){
                    let razdvojeniSmsStart = this.raspored[i].start.split(':');
                    let razdvojeniSmsEnd = this.raspored[i].end.split(':');
                    let start = razdvojeniSmsStart[0] * 60 + razdvojeniSmsStart[1]; //izraz neće biti okarakterisan kao izraz 
                    let end = razdvojeniSmsEnd[0] * 60 + razdvojeniSmsEnd[1];       //razdveniSmsStart i End neće se smatrati kao vrijednost koja se trba dodati na dio izraza pored    
                    let trenutno = (datum.getHours() * 60) + datum.getMinutes();    //jedno od rješenja možemo postići množenjem sa 1 
                    if(trenutno >= start && trenutno < end){                        //ovo smo uradili na svakom dijelu coda gdje imamo istu situaciju
                        let ostatakVremena = end - trenutno;
                        let naziv = null;
                        var format = /[-]/;
                        if(format.test(this.raspored[i].naziv)){
                            naziv = this.raspored[i].naziv.split("-")[0];
                        }
                        else {
                            naziv = this.raspored[i].naziv.trim();
                        }
                        rezultat = naziv + " " + ostatakVremena;
                    } 
                }
            }
        }
        return rezultat;
    }
    dajSljedecuAktivnost(datumString, nazivGrupe){
        let razdvojiVrijeme = datumString.split('T');
        let raz = razdvojiVrijeme[0].split('-');
        let sms = razdvojiVrijeme[1].split(':');
        let rezultat = "Nastava je gotova za danas";
        let pamtiVrijeme = null;
        let ostatakVremena = null;
        let naziv = null;
        let datum = new Date(raz[2].trim()+"-"+raz[1].trim()+"-"+raz[0].trim()+"T"+sms[0].trim()+":"+sms[1].trim()+":"+sms[2].trim());
        for(let i=0 ; i<this.raspored.length; i++){
            if(this.raspored[i].naziv.includes(nazivGrupe) || this.raspored[i].aktivnost==="predavanje"){
                let brojDan = this.trenutniDan(this.raspored[i].dan);
                if(brojDan===datum.getDay()){
                    let razdvojeniSmsStart = this.raspored[i].start.split(':');
                    let razdvojeniSmsEnd = this.raspored[i].end.split(':');
                    let start = razdvojeniSmsStart[0] * 60 + razdvojeniSmsStart[1];
                    let end = razdvojeniSmsEnd[0] * 60 + razdvojeniSmsEnd[1];
                    let trenutno = (datum.getHours() * 60) + datum.getMinutes();
                    if(trenutno < start){
                        if(pamtiVrijeme === null){
                            pamtiVrijeme = trenutno;
                            ostatakVremena = start - pamtiVrijeme;
                            var format = /[-]/;
                            if(format.test(this.raspored[i].naziv)){
                                naziv = this.raspored[i].naziv.split("-")[0];
                            }
                            else {
                                naziv = this.raspored[i].naziv.trim();
                            }
                            rezultat = naziv + " " + ostatakVremena;
                        }
                        else if((ostatakVremena)>=(start - trenutno)){
                            pamtiVrijeme = trenutno;
                            ostatakVremena = start - pamtiVrijeme;
                            var format = /[-]/;
                            if(format.test(this.raspored[i].naziv)){
                                naziv = this.raspored[i].naziv.split("-")[0];
                            }
                            else {
                                naziv = this.raspored[i].naziv.trim();
                            }
                            rezultat = naziv + " " + ostatakVremena;
                        }
                    } 
                }
            }
        }
        return rezultat;
    }
    dajPrethodnuAktivnost(datumString, nazivGrupe){
        let razdvojiVrijeme = datumString.split('T');
        let raz = razdvojiVrijeme[0].split('-');
        let sms = razdvojiVrijeme[1].split(':');
        let rezultat = null;
        let pamtiVrijeme = null;
        let ostatakVremena = null;
        let naziv = null;
        let brojac = 0;
        let datum = new Date(raz[2].trim()+"-"+raz[1].trim()+"-"+raz[0].trim()+"T"+sms[0].trim()+":"+sms[1].trim()+":"+sms[2].trim());
        let dan = datum.getDay();
        for(let i=0 ; i<this.raspored.length; i++){
            if(this.raspored[i].naziv.includes(nazivGrupe) || this.raspored[i].aktivnost==="predavanje"){
                let brojDan = this.trenutniDan(this.raspored[i].dan);
                if(brojDan===dan && brojac===0){
                    let razdvojeniSmsStart = this.raspored[i].start.split(':');
                    let razdvojeniSmsEnd = this.raspored[i].end.split(':');
                    let start = razdvojeniSmsStart[0] * 60 + razdvojeniSmsStart[1];
                    let end = razdvojeniSmsEnd[0] * 60 + razdvojeniSmsEnd[1];
                    let trenutno = (datum.getHours() * 60) + datum.getMinutes();
                    if(trenutno > end){
                        if(pamtiVrijeme === null){
                            pamtiVrijeme = trenutno;
                            ostatakVremena = pamtiVrijeme - end;
                            var format = /[-]/;
                            if(format.test(this.raspored[i].naziv)){
                                naziv = this.raspored[i].naziv.split("-")[0];
                            }
                            else {
                                naziv = this.raspored[i].naziv.trim();
                            }
                            rezultat = naziv + " " + ostatakVremena;
                        }
                        else if((ostatakVremena) >= (trenutno - end)){
                            pamtiVrijeme = trenutno;
                            ostatakVremena = pamtiVrijeme - end;
                            var format = /[-]/;
                            if(format.test(this.raspored[i].naziv)){
                                naziv = this.raspored[i].naziv.split("-")[0];
                            }
                            else {
                                naziv = this.raspored[i].naziv.trim();
                            }
                            rezultat = naziv + " " + ostatakVremena;
                        }
                    } 
                }
                if(brojDan===dan && brojac!==0){
                    let razdvojeniSmsStart = this.raspored[i].start.split(':');
                    let razdvojeniSmsEnd = this.raspored[i].end.split(':');
                    let start = razdvojeniSmsStart[0] * 60 + razdvojeniSmsStart[1];
                    let end = razdvojeniSmsEnd[0] * 60 + razdvojeniSmsEnd[1];
                    let trenutno = (datum.getHours() * 60) + datum.getMinutes();
                        if(pamtiVrijeme === null){
                            pamtiVrijeme = end;
                            ostatakVremena = ((24*60)-end) + ((brojac-1) * 24 * 60) + trenutno;
                            var format = /[-]/;
                            if(format.test(this.raspored[i].naziv)){
                                naziv = this.raspored[i].naziv.split("-")[0];
                            }
                            else {
                                naziv = this.raspored[i].naziv.trim();
                            }
                            rezultat = naziv + " " + ostatakVremena;
                        }
                        else if(end > pamtiVrijeme){
                            pamtiVrijeme = end;
                            ostatakVremena = ((24*60)-end) + ((brojac-1) * 24 * 60) + trenutno;
                            var format = /[-]/;
                            if(format.test(this.raspored[i].naziv)){
                                naziv = this.raspored[i].naziv.split("-")[0];
                            }
                            else {
                                naziv = this.raspored[i].naziv.trim();
                            }
                            rezultat = naziv + " " + ostatakVremena;
                            console.log(`sad ${rezultat}`);
                        }
                }
                if(i===this.raspored.length-1 && rezultat===null){
                    if(dan===1){
                        dan = 7;
                    }
                    else{
                        dan = dan - 1;
                    }
                    i=0;
                    brojac = brojac + 1;
                }
                if(brojac===7){
                    break;
                }
            }
        }
        return rezultat;
    }
}